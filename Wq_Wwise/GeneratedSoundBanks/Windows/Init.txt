Switch Group	ID	Name			Wwise Object Path	Notes
	2073579641	Music_Valse_Switch			\Default Work Unit\Music_Fd\Music_Valse_Switch	
	3075470356	Music_Combat_Switch			\Default Work Unit\Music_Fd\Music_Combat_Switch	

Switch	ID	Name	Switch Group			Notes
	1846755610	Small	Music_Valse_Switch			
	2849147824	Medium	Music_Valse_Switch			
	3522403288	Strong	Music_Valse_Switch			
	930712164	Off	Music_Combat_Switch			
	2191893976	Intensity_03	Music_Combat_Switch			
	2191893977	Intensity_02	Music_Combat_Switch			
	2191893978	Intensity_01	Music_Combat_Switch			

State Group	ID	Name			Wwise Object Path	Notes
	1404066300	Ambiances			\Default Work Unit\Ambiances	
	3261888876	Cinematique			\Default Work Unit\Cinematique	
	3589756548	Int_Ext			\Default Work Unit\Int_Ext	
	3923293925	Fight_Break			\Default Work Unit\Fight_Break	

State	ID	Name	State Group			Notes
	0	None	Ambiances			
	544496505	Calme	Ambiances			
	610107075	Tendu	Ambiances			
	2607556080	Menu	Ambiances			
	2764240573	Combat	Ambiances			
	0	None	Cinematique			
	529726532	End	Cinematique			
	916671257	Nope	Cinematique			
	0	None	Int_Ext			
	645492555	Out	Int_Ext			
	1752637612	In	Int_Ext			
	0	None	Fight_Break			
	514064485	Fight	Fight_Break			
	941442534	Break	Fight_Break			

Custom State	ID	Name	State Group	Owner		Notes
	540162831	Menu	Ambiances	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus		
	755606966	Calme	Ambiances	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\Ambiances		

Game Parameter	ID	Name			Wwise Object Path	Notes
	251412229	SHot			\Default Work Unit\SHot	
	504532776	Brick			\Factory Reflect Acoustic Textures\Textures\Brick	
	513139656	Mountain			\Factory Reflect Acoustic Textures\Textures\Mountain	
	841620460	Concrete			\Factory Reflect Acoustic Textures\Textures\Concrete	
	1449020017	GP_Sfx_Volume			\Default Work Unit\Menu\GP_Sfx_Volume	
	1755085759	Wood_Deep			\Factory Reflect Acoustic Textures\Textures\Wood_Deep	
	1873957695	Anechoic			\Factory Reflect Acoustic Textures\Textures\Anechoic	
	1970351858	Fabric			\Factory Reflect Acoustic Textures\Textures\Fabric	
	2058049674	Wood			\Factory Reflect Acoustic Textures\Textures\Wood	
	2412606308	Carpet			\Factory Reflect Acoustic Textures\Textures\Carpet	
	2492879145	GP_Music_Volume			\Default Work Unit\Menu\GP_Music_Volume	
	2637588553	Tile			\Factory Reflect Acoustic Textures\Textures\Tile	
	2928161104	Curtains			\Factory Reflect Acoustic Textures\Textures\Curtains	
	3195498748	Cork_Tiles			\Factory Reflect Acoustic Textures\Textures\Cork_Tiles	
	3651104317	GP_Sidechain_Sfx_Music			\Default Work Unit\GP_Sidechain_Sfx_Music	
	3670307564	Drywall			\Factory Reflect Acoustic Textures\Textures\Drywall	
	4168643977	Acoustic_Banner			\Factory Reflect Acoustic Textures\Textures\Acoustic_Banner	
	4262522749	Wood_Bright			\Factory Reflect Acoustic Textures\Textures\Wood_Bright	

Audio Bus	ID	Name			Wwise Object Path	Notes
	69765534	Musique			\Default Work Unit\Master Audio Bus\Musique	
	1069431850	Player			\Default Work Unit\Master Audio Bus\Player	
	1098341187	Pnj			\Default Work Unit\Master Audio Bus\Pnj	
	1404066300	Ambiances			\Default Work Unit\Master Audio Bus\Ambiances	
	2410751222	Golems			\Default Work Unit\Master Audio Bus\Pnj\Golems	
	2640763244	Sfx_Props_Shot			\Default Work Unit\Master Audio Bus\Sfx_Props_Shot	
	3025917224	Sfx_Power_Hand			\Default Work Unit\Master Audio Bus\Player\Sfx_Power_Hand	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	
	4166191657	Ennemies			\Default Work Unit\Master Audio Bus\Pnj\Ennemies	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	2157448753	Rvrb			\Default Work Unit\Master Audio Bus\Rvrb	

Effect plug-ins	ID	Name	Type				Notes
	251412229	Shot	Wwise Meter			
	586749551	Wwise_Meter_(Custom)	Wwise Meter			
	2907705636	New_Auro_Headphone	Auro Headphone			

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

